# jchemR
Wrapper functions for the commandline version of jchem (cxcalc).
More info about the descriptors that can be calculated can be found ath the [jchem docs](https://docs.chemaxon.com/display/CALCPLUGS/cxcalc+calculator+functions).

## Prerequisites
You need to install [jchem](https://www.chemaxon.com/download/jchem-suite/#jchem) and cxcalc needs to be in path and have the appropriate licenses to calculate the descriptors.


## Running in low priority
Open cxcalc.bat and change:

```
%JVMPATH% %JAVA_OPTS% -classpath %CLASSPATH% chemaxon.marvin.Calculator %*%
```

to 

```
start "" /low /b /wait %JVMPATH% %JAVA_OPTS% -classpath %CLASSPATH% chemaxon.marvin.Calculator %*%
```

## Installation
Install the devtools package and run:

```r
install.packages("devtools")
devtools::install_git("https://gitlab.com/R_packages/jchemR.git", args="--no-multiarch")
```
